import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;

class JobServiceTest {

	@BeforeAll
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	public static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	public void setUp() throws Exception {
	}

	@AfterEach
	public void tearDown() throws Exception {
	}

	@Test
	public void testCheckEnableTimeTodayIsBetweenStartTimeAndEndTime() {
		System.out.println("checkEnableTime Between startTime and endTime");
		// Arrange
		LocalDate startTime = LocalDate.of(2021, 1, 31);
		LocalDate endTime = LocalDate.of(2021, 2, 5);
		LocalDate today = LocalDate.of(2021, 2, 3);
		boolean expResult = true;
		// Act
		boolean result = JobService.checkEnableTime(startTime, endTime, today);
		// Assert
		assertEquals(expResult, result);
	}
	@Test
	public void testCheckEnableTimeTodayIsBeforeStartTime() {
		System.out.println("checkEnableTime today is after startTime");
		// Arrange
		LocalDate startTime = LocalDate.of(2021, 1, 31);
		LocalDate endTime = LocalDate.of(2021, 2, 5);
		LocalDate today = LocalDate.of(2021, 1, 18);
		boolean expResult = false;
		// Act
		boolean result = JobService.checkEnableTime(startTime, endTime, today);
		// Assert
		assertEquals(expResult, result);

	}
	@Test
	public void testCheckEnableTimeTodayIsAfterEndTime() {
		System.out.println("checkEnableTime today is after endTime");
		// Arrange
		LocalDate startTime = LocalDate.of(2021, 1, 31);
		LocalDate endTime = LocalDate.of(2021, 2, 5);
		LocalDate today = LocalDate.of(2021, 2, 6);
		boolean expResult = false;
		// Act
		boolean result = JobService.checkEnableTime(startTime, endTime, today);
		// Assert
		assertEquals(expResult, result);

	}
	@Test
	public void testCheckEnableTimeTodayIsEqualStartTime() {
		System.out.println("checkEnableTime today is equal startTime");
		// Arrange
		LocalDate startTime = LocalDate.of(2021, 1, 31);
		LocalDate endTime = LocalDate.of(2021, 2, 5);
		LocalDate today = LocalDate.of(2021, 1, 31);
		boolean expResult = true;
		// Act
		boolean result = JobService.checkEnableTime(startTime, endTime, today);
		// Assert
		assertEquals(expResult, result);

	}
	@Test
	public void testCheckEnableTimeTodayIsEqualEndTime() {
		System.out.println("checkEnableTime today is equal endTime");
		// Arrange
		LocalDate startTime = LocalDate.of(2021, 1, 31);
		LocalDate endTime = LocalDate.of(2021, 2, 5);
		LocalDate today = LocalDate.of(2021, 2, 5);
		boolean expResult = true;
		// Act
		boolean result = JobService.checkEnableTime(startTime, endTime, today);
		// Assert
		assertEquals(expResult, result);

	}

}
